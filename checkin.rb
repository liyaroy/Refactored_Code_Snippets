# some methods in checkin model
class Checkin < ActiveRecord::Base
  include Checkins::CheckinExtension
  def create_notification(employee, method_name, delete_description = '')
    notification_params = { emp_name: employee.emp_name, id: id, name: name,
                            delete_description: delete_description,
                            method_name: method_name }
    new_notification = Notification.new(notification_params)
    notification_hash = new_notification.notification_hash
    email_params, fcm_params = email_and_fcm_params(notification_hash)
    recipients = find_recipients(method_name, employee)
    notify(recipients, email_params, fcm_params)
  end

  def find_recipients(method_name, employee)
    if %w(create update destroy).include?(method_name)
      find_actors(employee)
    elsif method_name == 'overdue'
      [employee]
    else
      [employee, employee.superior].flatten.compact.uniq
    end
  end

  def email_and_fcm_params(notification_hash)
    email_params = { subject: notification_hash[:subject],
                     content: notification_hash[:content],
                     link: notification_hash[:link] }
    fcm_params = { event: notification_hash[:event],
                   title: notification_hash[:title],
                   assigned_employee_id: "#{assigned_employees.map(&:id)[0]}" }
    [email_params, fcm_params]
  end
end
